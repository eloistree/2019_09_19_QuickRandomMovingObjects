﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    public Vector3 m_rotation;
    public float m_speed=1;
   

    void Update()
    {
        transform.Rotate(m_rotation * Time.deltaTime* m_speed);
    }

    private void Reset()
    {
        m_rotation = new Vector3(Random.value * 360f, Random.value * 360f, Random.value * 360f);
    }
}
